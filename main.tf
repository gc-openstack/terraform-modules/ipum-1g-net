terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
  }
}

resource "openstack_networking_network_v2" "ipum_network" {
  name       = var.ipum_net_name
  mtu        = 1500
  dns_domain = "${var.ipum_net_name}.${var.dns_base}"
  segments {
    physical_network = var.ipum_physnet
    segmentation_id  = var.ipum_vlan
    network_type     = "vlan"
  }
}

# TODO: some questions...
# What about subnet per IPUM POD64?
# Is 10.1.255.x free?

resource "openstack_networking_subnet_v2" "bmc_subnet" {
  name       = "${var.ipum_net_name}-bmc"
  network_id = openstack_networking_network_v2.ipum_network.id
  cidr       = "10.1.0.0/16"
  ip_version = 4
  no_gateway = true
  allocation_pool {
    start = "10.1.255.100"
    end   = "10.1.255.199"
  }
}

resource "openstack_networking_subnet_v2" "gw_subnet" {
  name       = "${var.ipum_net_name}-gw"
  network_id = openstack_networking_network_v2.ipum_network.id
  cidr       = "10.2.0.0/16"
  ip_version = 4
  no_gateway = true
  allocation_pool {
    start = "10.2.255.100"
    end   = "10.2.255.199"
  }
}
