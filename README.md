# ipum-1g-net
Terraform Module: ipum-1g-net
=============================

This module generated a global (all vPOD) 1GbE network, connected to each IPUM GW and BMC.   

Within the network, two subnets are created:
* 10.1.0.0/16 - BMC network
* 10.2.0.0/16 - GW network

Variables
=========
* ipum_vlan - the VLAN to use (no default)
* ipum_net_name - the Openstack name of the network (default: ipum-1g)
* dns_base - the DNS base to use for this network (default: openstack.example.net)
* ipum_physnet - which physical network this network should be created on (default: physnet1)

Other Inputs
============
none

Usage
=====
The module is used in a single Terraform play, setup once when the first IPUM POD is integrated.  

    # Create all the networks
    module "network" {
        source    = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/ipum-1g-net.git"
        ipum_vlan = 82
        dns_base  = "openstack.acme.net"
    }

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.