variable "ipum_vlan" {
  type = number
}

variable "ipum_net_name" {
  default = "ipum-1g"
}

variable "dns_base" {
  type    = string
  default = "openstack.example.net"
}

variable "ipum_physnet" {
  default = "physnet1"
}
