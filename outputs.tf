output "network_id" {
  value       = openstack_networking_network_v2.ipum_network.id
  description = "IPUM network id"
}

output "bmc_subnet_id" {
  value       = openstack_networking_subnet_v2.bmc_subnet.id
  description = "IPUM BMC subnet id"
}

output "gw_subnet_id" {
  value       = openstack_networking_subnet_v2.gw_subnet.id
  description = "IPUM Gateway subnet id"
}
